package entities

type UserLoginResponseDTO struct {
    Token string `json:"token"`
}
