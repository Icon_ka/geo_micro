package service

import (
    "gitlab.com/Icon_ka/microservices/auth/internal/entities"
    "context"
    "log"
    "github.com/go-chi/jwtauth"
)

type UserService struct{}

func NewService() *UserService {
    return &UserService{}
}

func (u *UserService) Register(ctx context.Context, user *entities.User) error {
    var tokenAuth *jwtauth.JWTAuth
    tokenAuth = jwtauth.New("HS256", []byte("secret"), nil)

    _, str, err := tokenAuth.Encode(map[string]interface{}{"email": user.Email})
    if err != nil {
        return err
    }
    log.Print(str)

    return nil
}

func (u *UserService) Login(ctx context.Context, user *entities.User) (*entities.UserLoginResponseDTO, error) {

    return nil, nil
}
