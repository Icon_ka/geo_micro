package service

import (
    "context"
    "gitlab.com/Icon_ka/microservices/auth/internal/entities"
)

type UserServicer interface {
    Register(ctx context.Context, user *entities.User) error
    Login(ctx context.Context, user *entities.User) (*entities.UserLoginResponseDTO, error)
}
