package auth

import (
    auth "gitlab.com/Icon_ka/microservices/auth/protos/gen/go"
    "google.golang.org/grpc"
    "context"
    "log"
    "net"
    "gitlab.com/Icon_ka/microservices/auth/internal/service"
    "gitlab.com/Icon_ka/microservices/auth/internal/entities"
)

type serverAPI struct {
    auth.AuthServer
    service service.UserServicer
}

func NewServer() {
    listen, err := net.Listen("tcp", "localhost:11011")
    if err != nil {
        log.Fatalf("Ошибка при прослушивании порта: %v", err)
    }
    s := service.NewService()
    server := grpc.NewServer()
    RegisterServer(server, s)

    log.Println("Запуск gRPC сервера...")
    if err := server.Serve(listen); err != nil {
        log.Fatalf("Ошибка при запуске сервера: %v", err)
    }
}

func RegisterServer(gRPC *grpc.Server, service service.UserServicer) {
    auth.RegisterAuthServer(gRPC, &serverAPI{service: service})
}

func (s *serverAPI) Register(ctx context.Context, user *auth.RegisterRequest) (*auth.RegisterResponse, error) {
    err := s.service.Register(ctx, &entities.User{
        Email:    user.Email,
        Password: user.Password,
    })

    if err != nil {
        return nil, err
    }

    return &auth.RegisterResponse{Message: "Success"}, nil
}

func (s *serverAPI) Login(ctx context.Context, user *auth.LoginRequest) (*auth.LoginResponse, error) {
    panic("implement")
}
