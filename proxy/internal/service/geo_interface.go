package geo

import (
    "gitlab.com/Icon_ka/geo/internal/entities"
)

type AddressServicer interface {
    GetAddresses(req entities.SearchRequest) (*entities.Addresses, error)
    GeoCode(req entities.GeocodeRequest) (*entities.Addresses, error)
}
