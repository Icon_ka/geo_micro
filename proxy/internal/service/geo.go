package geo

import (
    "sync"
    "gitlab.com/Icon_ka/microservices/proxy/internal/entities"
    geo "gitlab.com/Icon_ka/microservices/geo/protos/gen/go"
)

type AddressService struct {
    geoCLI geo.DadataGRPCClient
    mutex  sync.Mutex
}

func NewAddressService() *AddressService {
    return &AddressService{
       geoCLI:
    }
}

func (a *AddressService) GetAddresses(req entities.SearchRequest) (*entities.Addresses, error) {
    a.mutex.Lock()
    defer a.mutex.Unlock()


    return &addressResponse, nil
}

func (a *AddressService) GeoCode(req entities.GeocodeRequest) (*entities.Addresses, error) {
    a.mutex.Lock()
    defer a.mutex.Unlock()


    return addr, nil
}
