package geo

import (
    "encoding/json"
    "net/http"
    "gitlab.com/Icon_ka/microservices/proxy/internal/entities"
)

type GeoCodeController struct {
    Service service.Service
}

func NewGeoCodeController(serv service.Service) *GeoCodeController {
    return &GeoCodeController{
        Service: serv,
    }
}

func (g *GeoCodeController) Geocode(w http.ResponseWriter, r *http.Request) {
    var re entities.GeocodeRequest
    err := json.NewDecoder(r.Body).Decode(&re)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    resp, err := g.Service.AddressService.GeoCode(re)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    err = json.NewEncoder(w).Encode(resp)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
}

func (g *GeoCodeController) Search(w http.ResponseWriter, r *http.Request) {
    var re entities.SearchRequest
    err := json.NewDecoder(r.Body).Decode(&re)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    resp, err := g.Service.AddressService.GetAddresses(re)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    err = json.NewEncoder(w).Encode(resp)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
}
