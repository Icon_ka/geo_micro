package geo

import (
    "net/http"
)

type GeoCoder interface {
    Geocode(http.ResponseWriter, *http.Request)
    Search(http.ResponseWriter, *http.Request)
}
