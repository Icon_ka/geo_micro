package main

import (
    "syscall"
    "os/signal"
    "os"
    "gitlab.com/Icon_ka/microservices/geo/internal/grpc/geo"
)

func main() {
    go func() {
        geo.NewServer()
    }()

    stop := make(chan os.Signal, 1)
    signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT)

    <-stop
}
