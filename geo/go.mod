module gitlab.com/Icon_ka/microservices/geo

go 1.19

require (
	github.com/golang-migrate/migrate/v4 v4.16.2
	github.com/gomodule/redigo v1.8.9
	github.com/mattes/migrate v3.0.1+incompatible
	google.golang.org/grpc v1.60.0
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/lib/pq v1.10.2 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	golang.org/x/net v0.16.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20231002182017-d307bd883b97 // indirect
)
