package geocoder

import (
    "fmt"
    "net/http"
    "log"
    "strings"
    "encoding/json"
    "gitlab.com/Icon_ka/microservices/geo/internal/entities"
)

const (
    search  = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address"
    geocode = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address"
)

func Search(request entities.SearchRequest) (*entities.Addresses, error) {
    client := &http.Client{}
    jsonData, err := json.Marshal(request)
    if err != nil {
        return nil, err
    }

    var data = strings.NewReader(string(jsonData))

    req, err := http.NewRequest("POST", search, data)
    if err != nil {
        return nil, err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")

    resp, err := client.Do(req)
    if err != nil {
        return nil, err
    }

    if resp.StatusCode != http.StatusOK {
        log.Println("Error:", resp.Status)
        return nil, fmt.Errorf("unexpected status: %s", resp.Status)
    }
    defer resp.Body.Close()

    var addr entities.Addresses
    err = json.NewDecoder(resp.Body).Decode(&addr)
    if err != nil {
        return nil, err
    }

    return &addr, nil
}

func Geocode(request entities.GeocodeRequest) (*entities.Addresses, error) {
    client := &http.Client{}
    jsonData, err := json.Marshal(request)
    if err != nil {

        return nil, err
    }

    var data = strings.NewReader(string(jsonData))
    req, err := http.NewRequest("POST", geocode, data)
    if err != nil {
        return nil, err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")
    resp, err := client.Do(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    var addr entities.Addresses
    err = json.NewDecoder(resp.Body).Decode(&addr)
    if err != nil {
        return nil, err
    }

    return &addr, nil
}
