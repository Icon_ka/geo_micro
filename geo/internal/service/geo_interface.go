package service

import "gitlab.com/Icon_ka/microservices/geo/internal/entities"

type AddressServicer interface {
    GetAddresses(req entities.SearchRequest) (*entities.Addresses, error)
    GeoCode(req entities.GeocodeRequest) (*entities.Addresses, error)
}
