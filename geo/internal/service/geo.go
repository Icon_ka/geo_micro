package service

import (
    "sync"
    "database/sql"
    "gitlab.com/Icon_ka/microservices/geo/internal/entities"
    geo "gitlab.com/Icon_ka/microservices/geo/internal/repository"
    "gitlab.com/Icon_ka/microservices/geo/internal/repository/cache"
    "gitlab.com/Icon_ka/microservices/geo/internal/geocoder"
)

type AddressService struct {
    cache cache.PostgresAddressRepositoryProxy
    mutex sync.Mutex
}

func NewAddressService(pg *sql.DB) *AddressService {
    repo := geo.NewPostgresAddressRepository(pg)
    return &AddressService{
        cache: *cache.NewPostgresAddressRepositoryProxy(repo),
    }
}

func (a *AddressService) GetAddresses(req entities.SearchRequest) (*entities.Addresses, error) {
    a.mutex.Lock()
    defer a.mutex.Unlock()

    address, err := a.cache.GetAddresses(req.Query)

    if err != nil {
        if err == sql.ErrNoRows {
            addr, err := geocoder.Search(req)
            if err != nil {
                return nil, err
            }

            var addressDTO entities.AddressDTO
            addressDTO.Data = addr.Suggestions[0].Value
            addressDTO.Lat = addr.Suggestions[0].Data.Lat
            addressDTO.Lon = addr.Suggestions[0].Data.Lon

            err = a.cache.Repository.SaveAddresses(addressDTO, req.Query)
            if err != nil {
                return nil, err
            }

            return addr, nil
        }
        return nil, err
    }
    var addressResponse entities.Addresses
    s := make([]entities.Suggestion, 1)
    s[0].Value = address.Data
    s[0].Data.Lat = address.Lat
    s[0].Data.Lon = address.Lon
    addressResponse.Suggestions = s

    return &addressResponse, nil
}

func (a *AddressService) GeoCode(req entities.GeocodeRequest) (*entities.Addresses, error) {
    a.mutex.Lock()
    defer a.mutex.Unlock()

    addr, err := geocoder.Geocode(req)
    if err != nil {
        return nil, err
    }

    return addr, nil
}
