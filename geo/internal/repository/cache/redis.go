package cache

import (
    "fmt"
    "github.com/gomodule/redigo/redis"
    "database/sql"
    geo "gitlab.com/Icon_ka/microservices/geo/internal/repository"
    "gitlab.com/Icon_ka/microservices/geo/internal/entities"
)

type PostgresAddressRepositoryProxy struct {
    Repository geo.AddressRepository
    cache      *redis.Pool
}

func NewPostgresAddressRepositoryProxy(repository geo.AddressRepository) *PostgresAddressRepositoryProxy {
    redisPool := &redis.Pool{
        Dial: func() (redis.Conn, error) {
            return redis.Dial("tcp", "redis:6379")
        },
    }

    return &PostgresAddressRepositoryProxy{
        cache:      redisPool,
        Repository: repository,
    }
}

func (r *PostgresAddressRepositoryProxy) GetAddresses(query string) (entities.AddressDTO, error) {
    conn := r.cache.Get()
    defer conn.Close()

    // Проверка наличия значения в кэше
    data, err := redis.String(conn.Do("GET", "data:"+query))
    lat, err := redis.String(conn.Do("GET", "lat:"+query))
    lon, err := redis.String(conn.Do("GET", "lon:"+query))

    if err == nil {
        var addressDTO entities.AddressDTO
        addressDTO.Data = data
        addressDTO.Lat = lat
        addressDTO.Lon = lon

        fmt.Println("From redis!")
        return addressDTO, nil
    } else if err != nil {
        // Значение отсутствует в кэше, делегируем запрос реальному объекту
        address, err := r.Repository.GetAddresses(query)
        if err == sql.ErrNoRows {
            return entities.AddressDTO{}, err
        }
        // Сохранение значения в кэше
        _, err = conn.Do("SET", "data:"+query, address.Data)
        _, err = conn.Do("SET", "lat:"+query, address.Lat)
        _, err = conn.Do("SET", "lon:"+query, address.Lon)
        if err != nil {
            return entities.AddressDTO{}, err
        }

        fmt.Println("From data base!")
        return address, nil
    }

    return entities.AddressDTO{}, err
}
