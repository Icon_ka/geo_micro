package geo

import (
    geo "gitlab.com/Icon_ka/microservices/geo/protos/gen/go"
    "net"
    "log"
    "google.golang.org/grpc"
    "context"
    "gitlab.com/Icon_ka/microservices/geo/internal/service"
    "gitlab.com/Icon_ka/microservices/geo/internal/entities"
    "bytes"
    "encoding/json"
    "fmt"
    "database/sql"
    "github.com/golang-migrate/migrate/v4/database/postgres"
    "github.com/golang-migrate/migrate/v4"
    _ "github.com/golang-migrate/migrate/v4/source/file"
    _ "github.com/mattes/migrate/source/file"
)

type serverApi struct {
    geo.DadataGRPCServer
    service service.AddressServicer
}

func NewServer() {
    db, err := sql.Open("postgres", fmt.Sprintf("postgres://alex:12345@db:5432/postgres?sslmode=disable"))
    if err != nil {
        log.Fatalf("Failed to open database: %v", err)
    }

    driver, err := postgres.WithInstance(db, &postgres.Config{})
    if err != nil {
        log.Fatalf("driver, err := postgres.WithInstance(db, &postgres.Config{}): %v", err)
    }

    m, err := migrate.NewWithDatabaseInstance(
        "file://./internal/migrations",
        "postgres",
        driver,
    )
    if err != nil {
        log.Fatalf("m, err := migrate.NewWithDatabaseInstance: %v", err)
    }

    err = m.Up()
    if err != nil {
        log.Fatalf("err = m.Up(): %v", err)
    }

    listen, err := net.Listen("tcp", "geo:22022")
    if err != nil {
        log.Fatalf("Ошибка при прослушивании порта: %v", err)
    }

    servicer := service.NewAddressService(db)
    server := grpc.NewServer()
    Register(server, servicer)

    log.Println("Запуск gRPC сервера...")
    if err := server.Serve(listen); err != nil {
        log.Fatalf("Ошибка при запуске сервера: %v", err)
    }
}

func Register(gRPC *grpc.Server, servicer service.AddressServicer) {
    geo.RegisterDadataGRPCServer(gRPC, &serverApi{service: servicer})
}

func (d *serverApi) Search(ctx context.Context, request *geo.SearchRequest) (*geo.AddressResponse, error) {
    var req entities.SearchRequest
    req.Query = request.Query
    addr, err := d.service.GetAddresses(req)
    if err != nil {
        return nil, err
    }

    var buf bytes.Buffer

    var res geo.AddressResponse

    err = json.NewEncoder(&buf).Encode(addr)

    if err != nil {
        return nil, err
    }

    err = json.NewDecoder(&buf).Decode(&res)

    if err != nil {
        return nil, err
    }

    return &res, nil
}

func (d *serverApi) Geocode(ctx context.Context, request *geo.GeocodeRequest) (*geo.AddressResponse, error) {
    var req entities.GeocodeRequest
    req.Lat = request.Lat
    req.Lon = request.Lon
    addr, err := d.service.GeoCode(req)
    if err != nil {
        return nil, err
    }

    var buf bytes.Buffer

    var res geo.AddressResponse

    err = json.NewEncoder(&buf).Encode(addr)

    if err != nil {
        return nil, err
    }

    err = json.NewDecoder(&buf).Decode(&res)

    if err != nil {
        return nil, err
    }

    return &res, nil
}
